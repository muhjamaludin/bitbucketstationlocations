## Git Atlassian Tutorial

### Learning Git
- ~~bibucket cloud~~
- [ ] code review
- [ ] branching
- [ ] undoing

### Beginner
- [ ] version control
- [ ] code management
- [ ] what's git
- [ ] git for organization
- [ ] install
- [ ] ssh
- [ ] archive
- [ ] gitops
- [ ] cheatsheet

### Started
- [ ] set repository
- [ ] saving
- [ ] inspecting
- [ ] undoing
- [ ] rewrite

### Advanced
- [ ] merging vs rebasing
- [ ] resetting, checking out, reverting
- [ ] git log
- [ ] git hooks
- [ ] refs, reflog
- [ ] sub modules
- [ ] subtree
- [ ] large repository
- [ ] LFS
- [ ] GC
- [ ] prune
- [ ] bash
- [ ] dotfiles
- [ ] cherry pick
- [ ] git lc
- [ ] git show

### Collaborating
- [ ] sync
- [ ] pull request
- [ ] branches
- [ ] workflows

### Migrating
- [ ] SVN to git
- [ ] prepare
- [ ] convert
- [ ] synchrone
- [ ] share
- [ ] migrate